#!/bin/sh

service influxdb restart

service chronograf restart


influx -execute "CREATE DATABASE telegraf"
influx -execute "CREATE USER telegraf WITH PASSWORD 'password'"
influx -execute "GRANT ALL ON telegraf TO telegraf"

tail -f /dev/null
