# Les containers utilisés :

1 dockerfile par application  c'est à dire :
- InfluxDB
- Telegraf
- Nginx (site web)
- Nginx (Proxy)

Dans un premier temps, nous construisons les images :

`docker-compose -f docker-compose.build.yml build`

On construit le registry pour stocker ces images :

`docker-compose -f docker-compose.registry.yml up -d`


On se connecte au registry grâce à la commande :

`docker login -u infra -p infrapass localhost:5000`


On peut créer d'autres utilisateurs grâce à la commande suivante :

`docker run \--entrypoint htpasswd httpd:2 -Bbn user password >> auth/htpasswd`
  
On tag toutes les images pour que le registry les récupère :

`docker image tag projet-pro-golang_nginxapp localhost:5000/nginxapp`

`docker image tag projet-pro-golang_influxdb localhost:5000/influxdb`

`docker image tag projet-pro-golang_nginxproxy localhost:5000/nginxproxy`

`docker image tag projet-pro-golang_telegraf localhost:5000/telegraf`


Je souhaite les ajouter à mon registry sur gitlab également :

`docker image tag projet-pro-golang_nginxapp registry.gitlab.com/eltacohero/projet-pro-golang/nginxapp`

`docker image tag projet-pro-golang_influxdb registry.gitlab.com/eltacohero/projet-pro-golang/influxdb`

`docker image tag projet-pro-golang_nginxproxy registry.gitlab.com/eltacohero/projet-pro-golang/nginxproxy`

`docker image tag projet-pro-golang_telegraf registry.gitlab.com/eltacohero/projet-pro-golang/telegraf`


#### On push le tout sur les registry (docker en local et gitlab) :

`docker push localhost:5000/nginxapp`

`docker push localhost:5000/influxdb`

`docker push localhost:5000/nginxproxy`

`docker push localhost:5000/telegraf`

`docker push registry.gitlab.com/eltacohero/projet-pro-golang/nginxapp`

`docker push registry.gitlab.com/eltacohero/projet-pro-golang/influxdb`

`docker push registry.gitlab.com/eltacohero/projet-pro-golang/nginxproxy`

`docker push registry.gitlab.com/eltacohero/projet-pro-golang/telegraf`


On déploie nos containers grâce aux images sur notre registry Gitlab :
`docker-compose -f docker-compose.yml up -d`


Puis on scale les load-balancers et fail-overs :
`docker-compose scale nginxapp=6`
